# zig-varint
[![builds.sr.ht status](https://builds.sr.ht/~alva/zig-varint/commits/trunk.svg)](https://builds.sr.ht/~alva/zig-varint?)

Encoding and decoding of variable-length signed and unsigned integers
using the Unsigned Little Endian Base 128 (ULEB128).

## usage
There's functions for encoding and decoding using writers and readers,
as well as functions that work on slices.

```zig
var buf: [4]u8 = undefined;
const x: u64 = 42;
const encoded = try encodeVarUintSlice(&buf, x);
const decoded = try decodeVarUintSlice(encoded);
try testing.expectEqual(x, decoded);
```
