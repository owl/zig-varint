const std = @import("std");

/// Takes a `std.io.Writer`-like `writer` into which the encoded unsigned
/// varint is written.
pub fn encodeVarUint(writer: anytype, value: u64) !void {
    var x = value;

    while (0x80 <= x) {
        const b: u8 = @truncate(x);
        try writer.writeByte(b | 0x80);
        x >>= 7;
    }

    try writer.writeByte(@truncate(x));
}

/// Takes a `[]u8` slice into which the encoded unsigned
/// varint is written.
pub fn encodeVarUintSlice(out: []u8, value: u64) ![]const u8 {
    var fbs = std.io.fixedBufferStream(out);
    const writer = fbs.writer();
    try encodeVarUint(writer, value);
    return fbs.getWritten();
}

/// Takes a `std.io.Writer`-like `writer` into which the encoded signed
/// varint is written.
pub fn encodeVarInt(writer: anytype, value: i64) !void {
    const ux: u64 = @bitCast((2 * value) ^ (value >> 63));
    return encodeVarUint(writer, ux);
}

/// Takes a `[]u8` slice into which the encoded signed
/// varint is written.
pub fn encodeVarIntSlice(out: []u8, value: i64) ![]const u8 {
    var fbs = std.io.fixedBufferStream(out);
    const writer = fbs.writer();
    try encodeVarInt(writer, value);
    return fbs.getWritten();
}

pub const DecodeError = error{Overflow};

/// Takes a `std.io.Reader`-like `reader` from which a signed varint is read,
/// decoded, and returned.
pub fn decodeVarInt(reader: anytype) !i64 {
    const ux = try decodeVarUint(reader);
    const x: i64 = @intCast(ux >> 1);
    const y: i64 = @intCast(ux & 1);
    return x ^ -y;
}

/// Takes a `[]const u8` slice from which a signed varint is read,
/// decoded, and returned.
pub fn decodeVarIntSlice(in: []const u8) !i64 {
    var fbs = std.io.fixedBufferStream(in);
    const reader = fbs.reader();
    return decodeVarInt(reader);
}

/// Takes a `std.io.Reader`-like `reader` from which an unsigned varint is
/// read, decoded, and returned.
pub fn decodeVarUint(reader: anytype) !u64 {
    var x: u64 = 0;
    var s: u6 = 0;

    for (0..std.math.maxInt(u4)) |i| {
        const b = try reader.readByte();
        const orm: u64 = @intCast(b & 0x7f);
        x |= orm << s;

        if (b < 0x80) {
            if (9 < i or i == 9 and 1 < b)
                return DecodeError.Overflow;

            return x;
        }

        const ov = @addWithOverflow(s, 7);

        if (0 != ov[1])
            return DecodeError.Overflow;

        s = ov[0];
    }

    return 0;
}

pub fn decodeVarUintSlice(in: []const u8) !u64 {
    var fbs = std.io.fixedBufferStream(in);
    const reader = fbs.reader();
    return decodeVarUint(reader);
}

const io = std.io;
const testing = std.testing;

test "decode variable uint" {
    var fbs = io.fixedBufferStream("\x2a");
    const reader = fbs.reader();
    const x = try decodeVarUint(&reader);
    try testing.expectEqual(x, 42);

    fbs = io.fixedBufferStream("\x80\x02");
    const y = decodeVarUint(fbs.reader());
    try testing.expectEqual(y, 0x100);
}

test "decode variable uint slice" {
    const x = try decodeVarUintSlice("\x2a"[0..]);
    try testing.expectEqual(x, 42);

    const y = decodeVarUintSlice("\x80\x02"[0..]);
    try testing.expectEqual(y, 0x100);
}

test "decode variable uint overflow 1" {
    const buf = "\xff\xff\xff\xff\xff\xff\xff\xff\xff\x02";
    var fbs = io.fixedBufferStream(buf);
    const reader = fbs.reader();
    const x = decodeVarUint(reader);
    try testing.expectError(DecodeError.Overflow, x);
}

test "decode variable uint overflow 2" {
    const buf = "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\x00";
    var fbs = io.fixedBufferStream(buf);
    const reader = fbs.reader();
    const x = decodeVarUint(reader);
    try testing.expectError(DecodeError.Overflow, x);
}

test "encode variable uint" {
    var buf: [4]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const writer = fbs.writer();
    try encodeVarUint(writer, 42);
    try testing.expectEqual(fbs.getWritten()[0], 42);

    try encodeVarUint(writer, 0x100);
    try testing.expectEqual(fbs.getWritten()[1], 128);
    try testing.expectEqual(fbs.getWritten()[2], 2);
}

test "encode variable uint slice" {
    var buf: [4]u8 = undefined;
    var slice = try encodeVarUintSlice(&buf, 42);
    try testing.expectEqual(slice[0], 42);

    slice = try encodeVarUintSlice(&buf, 0x100);
    try testing.expectEqual(slice[0], 128);
    try testing.expectEqual(slice[1], 2);
}

test "encode variable int" {
    var buf: [4]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const writer = fbs.writer();
    try encodeVarInt(writer, 42);
    try testing.expectEqual(fbs.getWritten()[0], 42 << 1);
}

test "encode variable int slice" {
    var buf: [4]u8 = undefined;
    const slice = try encodeVarIntSlice(&buf, 42);
    try testing.expectEqual(slice[0], 42 << 1);
}

test "round trip variable uint" {
    var buf: [4]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const reader = fbs.reader();
    const writer = fbs.writer();
    try encodeVarUint(writer, 0x10000);
    fbs = io.fixedBufferStream(fbs.getWritten());
    const res = try decodeVarUint(reader);
    try testing.expectEqual(res, 0x10000);
}

test "round trip variable int 1" {
    var buf: [4]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const reader = fbs.reader();
    const writer = fbs.writer();
    try encodeVarInt(writer, -0x10000);
    fbs = io.fixedBufferStream(fbs.getWritten());
    const res = try decodeVarInt(reader);
    try testing.expectEqual(res, -0x10000);
}

test "round trip variable int 2" {
    var buf: [4]u8 = undefined;
    var fbs = io.fixedBufferStream(&buf);
    const reader = fbs.reader();
    const writer = fbs.writer();
    try encodeVarInt(writer, 0x10000);
    fbs = io.fixedBufferStream(fbs.getWritten());
    const res = try decodeVarInt(reader);
    try testing.expectEqual(res, 0x10000);
}

test "readme example" {
    var buf: [4]u8 = undefined;
    const x: u64 = 42;
    const encoded = try encodeVarUintSlice(&buf, x);
    const decoded = try decodeVarUintSlice(encoded);
    try testing.expectEqual(x, decoded);
}
